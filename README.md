## Continuation to the Basic guide to write Gentoo Ebuilds

This is the continuation to the Basic guide to write Gentoo Ebuilds. We will ex
pand on the hello world ebuild by adding more advanced features.

# **Update 1: using `src_prepare()`**

In this example, we add the `src_prepare()` function where we will patch the 
package such that the shell script asks for the user's name and uses it instead 
of `world`.

# To be continued...
